package redroid.base;

import android.content.Context;
import android.support.v4.app.Fragment;

import redroid.widget.LoadingDialog;


/**
 * Fragment基类
 *
 * @author RobinVanYang created at 2017-06-26 14:24.
 */

public class BaseFragment extends Fragment {
    public Context mContext;
    private LoadingDialog mLoadingDialog;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    public void showLoadingDialog() {
        if (mLoadingDialog == null) mLoadingDialog = new LoadingDialog();

        mLoadingDialog.setText(getString(redroid.R.string.data_loading));

        mLoadingDialog.show(getChildFragmentManager());
    }

    public void hideLoadingDialog() {
        if (null != mLoadingDialog) mLoadingDialog.dismiss();
    }

}
