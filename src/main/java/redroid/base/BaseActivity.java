package redroid.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import redroid.R;


/**
 *
 * @author RobinVanYang
 * @createTime 2016-06-16 00:43
 */
public abstract class BaseActivity extends AppCompatActivity {
    public final String TAG = getClass().getName();
    protected Context mContext;
    protected Toolbar mToolbar;

    @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;

    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        setSupportActionBar(findViewById(R.id.toolbar));
    }

    public void showActionBarHome() {
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public void setTitle(int titleId) {
        if (null != mToolbar) {
            mToolbar.setTitle(titleId);
        } else {
            super.setTitle(titleId);
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        if (null != mToolbar) {
            mToolbar.setTitle(title);
        } else {
            super.setTitle(title);
        }
    }

    public void setTitleDrawableRight(@DrawableRes int drawableRight) {
        ((redroid.view.Toolbar)mToolbar).setTitleDrawableRight(drawableRight);
    }

    public void setTitleOnSelectListener(redroid.view.Toolbar.OnTitleSelectListener listener) {
        ((redroid.view.Toolbar)mToolbar).setOnTitleSelectListener(listener);
    }

    public void performTitleSelected(boolean isSelected) {
        ((redroid.view.Toolbar)mToolbar).performTitleSelected(isSelected);
    }

    @Override
    public void setSupportActionBar(@Nullable Toolbar toolbar) {
        if (null == mToolbar && null != toolbar) {
            super.setSupportActionBar(toolbar);
            mToolbar = toolbar;
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

    public void setHomeAsUpIndicator(@DrawableRes int drawableRes) {
        if (null != getSupportActionBar()) {
            getSupportActionBar().setHomeAsUpIndicator(drawableRes);
        }
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        switch (itemId) {
            case android.R.id.home:
                finish();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
